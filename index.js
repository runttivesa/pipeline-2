import express from 'express';
import "dotenv/config.js";

const server = express();

server.get('/', (req, res) => {

    res.send("response");
});

server.get('/secret', (req, res) => {

    res.send(`Secret: ${process.env.SECRET}`);
});

server.get('/version', (req, res) => {

    console.log(process.env.VERSION)
    res.send(`App Version: ${process.env.VERSION}`);
});

server.listen(3000, () => {
    console.log("Hello From Docker");
    console.log(`Build Version: ${process.env.BUILDVERSION}`);
});