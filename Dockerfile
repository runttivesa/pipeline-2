# DISTRO
FROM node:20-alpine

#APP DIR
WORKDIR /app

# COPY
COPY .env ./
COPY index.js ./
COPY package*.json ./

# NPM
RUN npm install

# ENV
ARG APP_BUILDVERSION=0.0.0
ENV BUILDVERSION=${APP_BUILDVERSION}
ENV SECRET=

# RUN
CMD ["node", "index.js"]
EXPOSE 3000